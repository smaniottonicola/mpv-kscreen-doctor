# mpv-kscreen-doctor

Automatically switch the display refresh rate to the best fitting one.

## Features

- [x] Set the screen refresh rate
- [x] Restore the previous rate when closing
- [x] Handle multiple screens
- [x] Handle multiple mpv instances
- [ ] Add options to configure the script
  - [ ] Allow changing the resolution

## Isn't there another script that already does this?

This plugin is heavily inspired by [mpv-plugin-xrandr](https://gitlab.com/lvml/mpv-plugin-xrandr), which is sadly not compatible with Wayland.

There is [wlr-randr](https://sr.ht/~emersion/wlr-randr/) that may help some, but Plasma is not supported (as of 04/2023).

For this reason I decided to create this, which should support both X11 and Wayland Plasma sessions.
